console.log("hello world")

//Assignment Operator
let assignmentNumber = 8;

//Arithmetic Operators
// + - * / % 


//Addition Assignment Operator(+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

//shorthand
assignmentNumber += 2;
console.log(assignmentNumber); //12

//Subtraction/Multiplication/division assignment operator (-=, *=, /=)
assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *= 2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);


//Multiple Operators and Parenthesis
/*
 -when multiple operators are applied in a single statement, it follows the PEMDAS(Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
 - The operations were done in the ff order:
 	1. 3 * 4 = 12
 	2. 12 / 5 = 2.4
 	3. 1 + 2 = 3
 	4. 3 - 2 = 1
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); //


let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);


//Increment and Decrement Operator
//Operators that add or subract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

//increment
//pre-fix incrementation.
//The value of "z" is added by a value of 1 before returning the value and storing it int the variable "increment".
++z;
console.log(z); //2 - the value of z was added with 1 and is immediately returned.

//post-fix incrementation
//The value of "z" is returned and stored in the variable "increment" then the value of 'z' is increased by one
z++;
console.log(z); //3 - The value of z was added with 1
console.log(z++); //3 - the previous value of the variable is returned.
console.log(z); //4 - a new value is no returned.

//pre-fix vs post-fix incrementation
console.log(z++); //4
console.log(z); //5

console.log(++z); //6 - the new value is returned immediately.


//pre-fix and post-fix decrementation
console.log(--z); //5 - with pre-fix decrementation the result of subtraction by 1 is returned immediately

console.log(z--); //5 - with post-fix decrementation the result of subtraction by q is not immediately returned, instead the previous value is returned first
console.log(z); //4



//Type Coercion
 //is the automatic or implicit conversion of values from one data type to another
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion);

//Adding/Contatenating a string and a number will result to a string


let numC = 16;
let numD = 14;


let nonCoercion = numC + numD;
console.log(nonCoercion)
console.log(typeof nonCoercion)

//the boolean "true" is also associated with the value of 1
let numE = true + 1;
console.log(numE); //
console.log(typeof numE); //number

//The boolean "false" is also associated with the value of 0
let numF = false + 1;
console.log(numF);



//Comparison Operators
let juan = 'juan';

// (==) Equality Operator != DATA TYPE
//checks whether the operands are equal/have the same content
//Attempts to CONVERT AND COMPARE operands of different data types
//Returns a boolean value

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
console.log('juan' == 'JUAN'); //false, case sensitive
console.log('juan' == juan); //true

//(!=) Inequality Operator
//checks whether the operands are not equal/have different content

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false);
console.log('juan' != 'JUAN');
console.log('juan' != juan);

//(===) Strictly Equality Operator
console.log('Strictly Equality Operator')
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false = not the same data type
console.log(0 === false); //false = different data typ
console.log('juan' === 'JUAN'); //false, case sensitive
console.log('juan' === juan); //true

//(!==) Strict Inequality Operator
console.log('Strictly Inequality Operator')
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'JUAN');
console.log('juan' !== juan);


//Relational Comparison Operators
//check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500"

console.log("Greater than")
//Greater than (>)
console.log(x > y); //false
console.log(w > y); //true

console.log("Less than")
//Less than (<)
console.log(w < x); //false
console.log(y < y); //false
console.log(x < 1000); //true
console.log(numString < 1000); //false - forced coercion 
console.log(numString < 6000); //true - forced coercion to change string to number
console.log(numString < "Jose"); //true

//Greater Than or Equal To
console.log (w >= 8000);

//Less Than or Equal To
console.log (x <= y);
console.log (y <= y);


//Logical Operators

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("Logical Operators")
//Logical And Operator (&& - Double Ampersand)
//Return true if all operand are true
let authorization1 = isAdmin && isRegistered; 
console.log(authorization1)

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2)

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3)//False

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4)//True



let userName = "gamer2022";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;
//.length is a property of strings which determines the number of characters in the string
console.log(registration1)

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2)

console.log("LOGICAL OR OPERATOR");
//OR Operator (|| - Double Pipe)
// returns true if atleast ONE of the operands are true

let userLevel1 = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement1); //True

let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(guildRequirement2);

console.log("Not Operator");
//NOT OPerator (!)
//turns a boolean into the opposites value

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //true

console.log(!isRegistered); //false
console.log(!isLegalAge); //false

let opposite = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite);
console.log(opposite2);

//if, else if, and else statement

//IF statement
//if statements will run a code block if the condition specified is tru or results to true



let numG = -1;
if (numG < 0){
    console.log('Hello');
} 

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if (userName3.length > 10){
    console.log('Welcome to Game Online!')
}

if (userLevel3 >= requiredLevel){
    console.log('You are qualified to join the guild');
}

if (userName3.length >= 10 && isRegistered && isAdmin){
    console.log('Thank you for joiniing the admin')
}

//ELSE statement
//The 'else' statement executes a block of codes if all other conditions are false

if (userName3.length >= 10 && userLevel3 >= requiredLevel >= requiredLevel && userAge3 >= requiredAge) {
    console.log('Thank you for joiniing the noobies guild!')

} else {
    console.log('You are too strong to be noob. :(')
}

//else if statement
if (userAge3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
    console.log('Thank you for joiniingthe noobies guild.')
} else if(userLevel3 > 25) {
    console.log('You are too strong to be a noob.')
} else if(userAge3 < requiredAge){
    console.log('You are too young to join the guild.')
} else {
    console.log('Better luck next time')
}

// if, else if and else statement with functions

function addNum(num1, num2){
    //check if the numbers being passed are number types.
    //typeof keyworf returns a string which tells the type of the data that follows it
    if(typeof num1 === "number" && typeof num2 === "number"){
        console.log('Run only if both arguments passed are number types');
        console.log(num1 + num2);
    } else {
        console.log('One or both of the arguments are not numbers');
    }
}

addNum(5, '2');

let numsample = 5;
console.log(typeof numsample);

//create log in function

function login(username, password){
    //check if the argument passed are strings
    if (typeof username === "string" && typeof password === "string"){
        console.log('Both arguments are strings.');

      





        /*nested if-else
            will run if the parent if statement is able to agree to accomplish its condition */

         if (username.length >= 8 && password.length >= 8) {
            console.log('Thank you for logging in')
         } else if (username.length <= 8){
            console.log('Username is too short')
         } else if (password.length <=8) {
            console.log('Password is too short')
         } else {
            console.log('Credentials too short')
         }
        
    } else {
        console.log('One of the arguments is not a string.');
    }
}

login('jane', 'jane123')

//function with return keyword

let message = 'No message.';
console.log(message);

function determingTyphoonIntensity(windspeed){
    if (windspeed < 30 ){
        return 'Not a typhoon yet.';
    }
    else if (windspeed <= 61) {
        return 'Tropical depression detected.';
    }
    else if (windspeed >= 62 && windspeed <= 88){
        return 'Tropical storm is detected.'
    }
    else if (windspeed >= 89 && windspeed <= 117){
        return 'Severe tropical storm detected.'
    }
    else{
        return 'Typhoon detected.'
    }
}

message = determingTyphoonIntensity(68);
console.log(message);

if (message == 'Tropical storm is detected.'){
    console.warn(message)
}

//Truthy and Falsy

if(true){
    console.log('Truthy')
}

if (1){
    console.log('True')
}

//falsy
if (false){
    console.log('Falsy')
}

if (0){
    console.log('False')
}

if ([]) {
    console.log('Truthy')
}

if (undefined){
    console.log('Falsy')
}

//Ternary Operator

/*
Syntax; 
    (expession/condition) ? ifTrue : ifFalse;
    Three operand of ternary operator:

    1. condition
    2. expression to execute if the condition is truthy
    3. expression to execute if the condition is false

    Ternary operators were created so that we can have an if-else statement in one line
    */

//sinle statement execution
let ternaryResults = (1 < 18) ? true : false;
console.log(`Results of ternary operator ${ternaryResults}`);

// if (1<18){
//     console.log(true)

// }
// else{
//     console.log(false)
// }

let price = 50000;

price > 1000 ? console.log ('price is over 1000') : console.log('price is less than 1000')

let villain = "Harvey Dent"

villain === 'Two Face'
? console.log('You lived long enough to be a villain')
: console.log('Not quite villanous yet.')

//Ternary operators have an implicit "return" statement that without return keyword, the resulting expression can be stored in a variable.


//else if ternary operator

let a = 7
a === 5
? console.log('A')
: (a === 10 ? console.log('A is 10') : console.log('A is not 5 or 10'))

//multiple statement execution

let name;

function isOfLegalage() {
    name = 'John'
    return 'You are of the legal age limit';

}

function isUnderAge() {
    name = 'Jane'
    return 'You are under the age limit';
}

// let age = parseInt(prompt('What is your age?'));
// console.log(age);

// ? = "if"
// () = condition
// : = else

// let legalAge = (age > 18) ? isOfLegalage() : isUnderAge();
// console.log(`Result of Ternary operator in functions: ${legalAge}, ${name}`);

//Mini-Activity

// let day = prompt ('Enter a day')

// colorOfTheDay(day)

// function colorOfTheDay(day){
//     if (typeof day === 'string'){
//         if (day.toLowerCase() === 'monday') {
//             alert(`Today is ${day}. Wear Black.`);
//         }
//         if (day.toLowerCase() === 'tuesday') {
//                     alert(`Today is ${day}. Wear Green.`);
//         }
//         if (day.toLowerCase() === 'wednesday') {
//             alert(`Today is ${day}. Wear Yellow.`);
//         }
//         if (day.toLowerCase() === 'Thursday') {
//             alert(`Today is ${day}. Wear Red.`);
//         }
//         if (day.toLowerCase() === 'friday') {
//             alert(`Today is ${day}. Wear Violet.`);
//         }
//         if (day.toLowerCase() === 'saturday') {
//             alert(`Today is ${day}. Wear Blue.`);
//         }
//         if (day.toLowerCase() === 'sunday') {
//             alert(`Today is ${day}. Wear White.`);
//         }
//         else{
//             alert ('Invalid Input.')
//         }

//     }else{
//         alert ('Invalid Input. Enter a valid day of the week.')
//     }

// }


// parseInt() - convert a string to be a number

//Switch Statement
/*
syntax:

    switch (expression/condition){
        case value:
            statement;
            break;

        defualt:
            statement;
            break;
    }

*/


// let hero = prompt('Type of hero').toLowerCase();

// switch (hero) {
//     case 'jose rizal':
//         console.log('Nationa; Hero of the Philippines');
//         break;
//     case 'george washington':
//         console.log('Hero of the American Revolution');
//         break;
//     case 'hercules':
//         console.log('Legendary Hero of the Greek');
//         break;
//     default:
//         console.log('Please type again');
//         break;
// }
    
// function roleChecker (role) {
//     switch(role){
//         case "admin":
//             console.log("Welcome Admin, to the dashboard.");
//             break;
//         case "user":
//             console.log("You are not authorized to view this page.");
//             break;
//         case "guest":
//             console.log("Go to the registration page to register.");
//             break;
//         default:
//             console.log("Invalid Role")
//             break;

//     }
// }

// roleChecker("admin")

//Try-Catch-Finally Statement
//this is used for error handling


function showIntensityAlert(windspeed){
    //Attempt to execute a code
    try {
        alerat(determineTyphoonIntensity(windspeed));
    }
    catch (error){
        console.log(typeof error);

        //catch errors within 'try' statement
        console.warn(error.message);
    }
    finally {
        alert("Intensity updates will show new alert")
    }
}


showIntensityAlert(68)

//throw - user-defined exception

const number = 40;
try {
    if(number > 50){
        console.log("Success");
    }
    else{
        throw Error("The number is low");
    }
    //
    console.log("Hello")
}
catch(error){
    console.log("An error caught");
    console.warn(error.message)
}
finally{
    console.log("Please add a higher number")
}

//another example





























